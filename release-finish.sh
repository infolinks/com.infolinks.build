#!/usr/bin/env bash

# enable debug if requested
[[ ${DEBUG} == "true" ]] && set -x

# get version
BUILD_SCRIPT_DIR=$(dirname ${BASH_SOURCE[0]})
VERSION=${1}
[[ -z ${VERSION} ]] && echo "please provide version" && exit 1

# create release branch
git flow release finish ${VERSION} --fetch --push --pushproduction --pushdevelop --pushtag --message "."
