#!/usr/bin/env bash

# enable debug if requested
[[ ${DEBUG} == "true" ]] && set -x

# infer version
BUILD_SCRIPT_DIR=$(dirname ${BASH_SOURCE[0]})
VERSION=$(${BUILD_SCRIPT_DIR}/release-version.sh)
[[ $? != 0 ]] && echo "failed inferring version" && exit 1

# infer target images name components
REGISTRY="gcr.io"
PROJECT="infolinks-dev"

# iterate every 'Dockerfile' found under current directory tree, and push it's image
ROOT=$(pwd)
pushd . > /dev/null
for docker_file in $(find . -name Dockerfile); do
    MODULE_DIR="${ROOT}/$(dirname ${docker_file})"
    cd ${MODULE_DIR}
    [[ $? != 0 ]] && echo "failed switching to directory '${MODULE_DIR}'" && exit 1

    IMAGE_NAME="${REGISTRY}/${PROJECT}/$(basename $(pwd)):${VERSION}"
    echo
    echo "PUSHING ${IMAGE_NAME} (using ${docker_file})"
    echo "-----------------------------------------------------------------------------------------------------------------------"
    gcloud docker --authorize-only
    [[ $? != 0 ]] && exit 1
    docker push ${IMAGE_NAME}
    [[ $? != 0 ]] && exit 1
done
popd > /dev/null
