#!/usr/bin/env bash

# enable debug if requested
[[ ${DEBUG} == "true" ]] && set -x

# infer branch
BRANCH="unknown"
if [[ ! -z ${BRANCH_NAME} ]]; then
    BRANCH=${BRANCH_NAME}
elif [[ ! -z ${BITBUCKET_BRANCH} ]]; then
    BRANCH=${BITBUCKET_BRANCH}
else
    BRANCH=$(git rev-parse --abbrev-ref HEAD)
fi

# infer tag from branch
TAG=${BRANCH}
TAG=$(echo -n ${TAG} | sed 's/^develop$/dev/')
TAG=$(echo -n ${TAG} | sed 's/^release\/\(.*\)$/rc-\1/')
TAG=$(echo -n ${TAG} | sed 's/^hotfix\/\(.*\)$/rc-\1/')
TAG=$(echo -n ${TAG} | sed 's/^feature\/\(.*\)$/feature-\1/')
TAG=$(echo -n ${TAG} | sed 's/^bugfix\/\(.*\)$/bugfix-\1/')

# add build-number if available
if [[ ! -z ${BITBUCKET_BUILD_NUMBER} ]]; then
    BUILD="-${BITBUCKET_BUILD_NUMBER}"
else
    BUILD=""
fi

# print version
echo -n "${TAG}${BUILD}"
