#!/bin/sh

OS=$(cat /etc/os-release | grep -E "^ID=" | sed 's/^ID=//' | sed 's/"//g')
[ $? -ne 0 ] && echo "failed inferring current operating system!" && exit 1

echo "Installing Git and SSH client..."
if [ "${OS}" = "alpine" ]; then
    apk add --no-progress --quiet --update git openssh-client
    [ $? -ne 0 ] && echo "failed installing Git!" && exit 1
elif [ "${OS}" = "ubuntu" ]; then
    apt-get -qq update && apt-get install -qqy git openssh-client
    [ $? -ne 0 ] && echo "failed installing Git!" && exit 1
elif [ "${OS}" = "centos" ]; then
    yum install -y git openssh-client
    [ $? -ne 0 ] && echo "failed installing Git!" && exit 1
else
    echo "Unsupported operating system: ${OS}"
    exit 1
fi

echo "Installing 'bash'..."
if [ "${OS}" = "alpine" ]; then
    apk add --no-progress --quiet --update bash
    [ $? -ne 0 ] && echo "failed installing Bash!" && exit 1
elif [ "${OS}" = "ubuntu" ]; then
    apt-get -qq update && apt-get install -qqy bash
    [ $? -ne 0 ] && echo "failed installing Bash!" && exit 1
elif [ "${OS}" = "centos" ]; then
    yum install -y bash
    [ $? -ne 0 ] && echo "failed installing Bash!" && exit 1
else
    echo "Unsupported operating system: ${OS}"
    exit 1
fi

echo "Cloning 'infolinks/com.infolinks.build.git' repository..."
git clone https://bitbucket.org/infolinks/com.infolinks.build.git --depth 1 /build
[ $? -ne 0 ] && echo "failed cloning Git repository!" && exit 1
exit 0
