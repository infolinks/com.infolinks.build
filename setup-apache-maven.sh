#!/bin/sh

# infer operating system
OS=$(cat /etc/os-release | grep -E "^ID=" | sed 's/^ID=//' | sed 's/"//g')
[ $? -ne 0 ] && echo "failed inferring current operating system!" && exit 1

echo "Installing gettext (required for 'envsubst')..."
if [ "${OS}" = "alpine" ]; then
    apk add --no-progress --quiet --update gettext
    [ $? -ne 0 ] && echo "failed installing 'gettext'!" && exit 1
elif [ "${OS}" = "ubuntu" ]; then
    apt-get -qq update && apt-get install -qqy gettext-base
    [ $? -ne 0 ] && echo "failed installing 'gettext'!" && exit 1
elif [ "${OS}" = "centos" ]; then
    yum install -y gettext
    [ $? -ne 0 ] && echo "failed installing 'gettext'!" && exit 1
else
    echo "Unsupported operating system: ${OS}"
    exit 1
fi

echo "Adding '/root/.m2/settings.xml'..."
[ -z ${ARTIFACTORY_USERNAME} ] && echo "\${ARTIFACTORY_USERNAME} not defined" && exit 1
[ -z ${ARTIFACTORY_PASSWORD} ] && echo "\${ARTIFACTORY_PASSWORD} not defined" && exit 1
curl -sSL https://goo.gl/7Kfkvc | envsubst > /root/.m2/settings.xml
[ $? -ne 0 ] && echo "failed adding/updating '/root/.m2/settings.xml'!" && exit 1

echo "Installing Apache Maven..."
mkdir -vp /usr/local/share/maven
[ $? -ne 0 ] && exit 1
MAVEN_HOME="/usr/local/share/maven"
MAVEN_VERSION="3.5.0"
MAVEN_DIST_URL="https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz"
curl -sSL ${MAVEN_DIST_URL} | tar xz -C ${MAVEN_HOME} --strip-components=1
[ $? -ne 0 ] && echo "failed extracting Apache Maven ${MAVEN_VERSION}!" exit 1
ln -s ${MAVEN_HOME}/bin/mvn /usr/local/bin/mvn
[ $? -ne 0 ] && echo "failed creating /usr/local/bin/mvn!" exit 1

# export back
export MAVEN_HOME
