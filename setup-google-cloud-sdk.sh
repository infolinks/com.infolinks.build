#!/bin/sh

[ -z ${GCP_SVC_ACCOUNT_JSON} ] && echo "missing GCP_SVC_ACCOUNT_JSON environment variable" && exit 1

OS=$(cat /etc/os-release | grep -E "^ID=" | sed 's/^ID=//' | sed 's/"//g')
[ $? -ne 0 ] && echo "failed inferring current operating system!" && exit 1

echo "Installing Python..."
if [ "${OS}" = "alpine" ]; then
    apk add --no-progress --quiet --update python py-pip
    [ $? -ne 0 ] && echo "failed installing Python!" && exit 1
elif [ "${OS}" = "ubuntu" ]; then
    apt-get -qq update && apt-get install -qqy python python-pip
    [ $? -ne 0 ] && echo "failed installing Python!" && exit 1
else
    echo "Unsupported operating system: ${OS}"
    exit 1
fi

echo "Installing Google Cloud SDK..."
GCLOUD_SDK_URL="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-168.0.0-linux-x86_64.tar.gz"
curl -sSL ${GCLOUD_SDK_URL} | tar xz -C /usr/local/share
[ $? -ne 0 ] && echo "failed installing Google Cloud SDK!" && exit 1

echo "Setting up Google Cloud SDK links in '/usr/local/bin'..."
ln -s /usr/local/share/google-cloud-sdk/bin/bq /usr/local/bin/bq
ln -s /usr/local/share/google-cloud-sdk/bin/docker-credential-gcloud /usr/local/bin/docker-credential-gcloud
ln -s /usr/local/share/google-cloud-sdk/bin/gcloud /usr/local/bin/gcloud
ln -s /usr/local/share/google-cloud-sdk/bin/git-credential-gcloud.sh /usr/local/bin/git-credential-gcloud.sh
ln -s /usr/local/share/google-cloud-sdk/bin/gsutil /usr/local/bin/gsutil

echo "Authenticating to Google Cloud..."
mkdir -pv /build
echo -n "${GCP_SVC_ACCOUNT_JSON}" > /build/gcloud_service_account.json
[ $? -ne 0 ] && echo "failed saving service account credentials!" && exit 1
gcloud auth activate-service-account --key-file /build/gcloud_service_account.json
[ $? -ne 0 ] && echo "failed authenticating to Google Cloud using provided service account!" && exit 1
GOOGLE_APPLICATION_CREDENTIALS="/build/gcloud_service_account.json"
export GOOGLE_APPLICATION_CREDENTIALS
