#!/bin/sh

OS=$(cat /etc/os-release | grep -E "^ID=" | sed 's/^ID=//' | sed 's/"//g')
[ $? -ne 0 ] && echo "failed inferring current operating system!" && exit 1

echo "Installing OpenJDK..."
if [ "${OS}" = "alpine" ]; then
    apk add --no-progress --quiet --update openjdk8
    [ $? -ne 0 ] && echo "failed installing OpenJDK!" && exit 1
    JAVA_HOME="/usr/lib/jvm/java-1.8-openjdk"
elif [ "${OS}" = "ubuntu" ]; then
    apt-get -qq update && apt-get install -qqy openjdk-8-jdk
    [ $? -ne 0 ] && echo "failed installing OpenJDK!" && exit 1
    JAVA_HOME="java-1.8.0-openjdk-amd64"
elif [ "${OS}" = "centos" ]; then
    yum install -y java-1.8.0-openjdk-devel
    [ $? -ne 0 ] && echo "failed installing OpenJDK!" && exit 1
    JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk"
else
    echo "Unsupported operating system: ${OS}"
    exit 1
fi
export JAVA_HOME
