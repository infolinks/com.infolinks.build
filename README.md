# Build environment

This project provides useful development scripts to be used during
build and/or development time - both by developers as well as by CI/CD
environments.

## Bootstrap

This script will initialize *a container* with the build environment. It
will clone the [com.infolinks.build](https://bitbucket.org/infolinks/com.infolinks.build)
repository to `/build` which will provide access to all the other
scripts this repository provides.

**DO NOT** run this on your development workstation!

The easiest way to invoke this script is as follows:

#### Alpine OS

    apk add --quiet --update curl \
        && curl -sSL https://bitbucket.org/infolinks/com.infolinks.build/raw/master/init.sh \
        | source /dev/stdin

#### Ubuntu

    apt-get update && apt-get install -y curl \
        && curl -sSL https://bitbucket.org/infolinks/com.infolinks.build/raw/master/init.sh \
        | source /dev/stdin

#### CentOS

    yum install -y curl \
        && curl -sSL https://bitbucket.org/infolinks/com.infolinks.build/raw/master/init.sh \
        | source /dev/stdin

### Short URL

You can replace the Bitbucket URL above with this one: https://goo.gl/qFEC98
